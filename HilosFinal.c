#include <fcntl.h>
#include <pthread.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>

#define SIZE 4096

typedef struct{
     unsigned long offset;
     unsigned long size;
}PART;
char file[50]; /* Origen */
char fileout1[50]; /* Destino */
char fileout2[50]; /* Destino */
char fileout3[50]; /* Destino */
char fileout4[50]; /* Destino */

void* func1(void *arg){
	int fin, fout1, fout2, fout3, fout4, x, i;
	PART *part;
	char data[SIZE];

	part = (PART *)arg;
	fin  = open(file, O_RDONLY);
	fout1 = open(fileout1, O_WRONLY);
	fout2 = open(fileout2, O_WRONLY);
	fout3 = open(fileout3, O_WRONLY);
	fout4 = open(fileout4, O_WRONLY);
	lseek(fin, part->offset, SEEK_SET);
	lseek(fout1, part->offset, SEEK_SET);
	lseek(fout2, part->offset, SEEK_SET);
	lseek(fout3, part->offset, SEEK_SET);
	lseek(fout4, part->offset, SEEK_SET);
	while(i < part->size){
	x = read(fin, data, SIZE);
	write(fout1, data, x);
	write(fout2, data, x);
	write(fout3, data, x);
	write(fout4, data, x);
	i += x;
   }

   printf("Hilo exitoso.\n");
   close(fout1);
   close(fout2);
   close(fout3);
   close(fout4);
   close(fin);
};

int main(int argc, char *argv[]){
char data[SIZE];
struct stat f_stat;
int fin1, fout1, fout2, fout3, fout4, x, chk, i=0;
PART part1, part2, part3, part4, part5, part6, part7, part8, part9, part10, part11, part12;
pthread_t t1, t2, t3, t4, t5, t6, t7, t8; // Estos hilos haran el trabajo 

clock_t t_ini, t_fin, t_date;
double secs;
char carpeta1[50];
char carpeta2[50];
char carpeta3[50];
char carpeta4[50];
if(argc < 3){
puts("No hay argumentos");
puts("parallelCopy origen destino destino destino destino");
return -1;
}
printf("Ingrese la direccion de la carpeta 1");
  scanf("%s",carpeta1);
  mkdir(carpeta1, 0777);

printf("Ingrese la direccion de la carpeta 2");
  scanf("%s",carpeta2);
  mkdir(carpeta2, 0777);

printf("Ingrese la direccion de la carpeta 3");
  scanf("%s",carpeta3);
  mkdir(carpeta3, 0777);

printf("Ingrese la direccion de la carpeta 4");
  scanf("%s",carpeta4);
  mkdir(carpeta4, 0777);
time_t tiempoAhora;
time(&tiempoAhora);
t_ini = clock();
printf("Hora inicio: %s",ctime(&tiempoAhora));

strcpy(file, argv[1]);
stat(file, &f_stat); // getting the meta info of file
strcpy(fileout1, argv[2]);
strcpy(fileout2, argv[3]);
strcpy(fileout3, argv[4]);
strcpy(fileout4, argv[5]);

printf("El tamaño del archivo es: %lu \n", f_stat.st_size);
part1.offset = 0;

//Para separar las partes para primera carpeta
part1.size = f_stat.st_size / 3;
part2.offset = part1.size;
part2.size = part1.size;
part3.offset = part2.offset + part2.size;
part3.size = f_stat.st_size - part3.offset;

//Para separar las partes para segunda carpeta
part1.size = f_stat.st_size / 3;
part5.offset = part4.size;
part5.size = part4.size;
part6.offset = part5.offset + part5.size;
part6.size = f_stat.st_size - part6.offset;

//Para separar las partes para Tercera carpeta 
part1.size = f_stat.st_size / 3;
part8.offset = part7.size;
part8.size = part7.size;
part9.offset = part8.offset + part8.size;
part9.size = f_stat.st_size - part9.offset;


//Para separar las partes para cuarta carpeta
part1.size = f_stat.st_size / 3;
part11.offset = part10.size;
part11.size = part10.size;
part12.offset = part11.offset + part11.size;
part12.size = f_stat.st_size - part12.offset;

fin1 = open(file, O_RDONLY);
fout1 = open(fileout1, O_WRONLY|O_CREAT, 0666);
fout2 = open(fileout2, O_WRONLY|O_CREAT, 0666);
fout3 = open(fileout3, O_WRONLY|O_CREAT, 0666);
fout4 = open(fileout4, O_WRONLY|O_CREAT, 0666);


/*aqui se crean los hilos*/
pthread_create(&t1, NULL, func1, &part2);
pthread_create(&t2, NULL, func1, &part3);
pthread_create(&t3, NULL, func1, &part5);
pthread_create(&t4, NULL, func1, &part6);
pthread_create(&t5, NULL, func1, &part8);
pthread_create(&t6, NULL, func1, &part9);
pthread_create(&t7, NULL, func1, &part11);
pthread_create(&t8, NULL, func1, &part12);
while(i < part1.size){
x = read(fin1, data, SIZE);
write(fout1, data, x);
write(fout2, data, x);
write(fout3, data, x);
write(fout4, data, x);
i += x;
}
pthread_join(t1, NULL); 
pthread_join(t2, NULL); 
pthread_join(t3, NULL); 
pthread_join(t4, NULL); 
pthread_join(t5, NULL);
pthread_join(t6, NULL);
pthread_join(t7, NULL);
pthread_join(t8, NULL);
printf("Archivo copiado");
close(fout1);
close(fout2);
close(fout3);
close(fout4);
close(fin1);
t_fin = clock();

secs = (t_fin - t_ini) / (double)CLOCKS_PER_SEC;
int mins = (int)secs/60;
int segs = (int)secs%60;
 printf("El tiempo de espera para copiar los archivos fue de:  %d minutos , %d  segundos \n ",mins,segs);
time(&tiempoAhora);
printf("Hora final: %s",ctime(&tiempoAhora));
return 0;
}

