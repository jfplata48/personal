
package webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AgregarFactura complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AgregarFactura"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="numeroF" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="fechaF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="montoF" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="codigoC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="formaPF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgregarFactura", propOrder = {
    "numeroF",
    "fechaF",
    "montoF",
    "codigoC",
    "formaPF"
})
public class AgregarFactura {

    protected int numeroF;
    protected String fechaF;
    protected int montoF;
    protected String codigoC;
    protected String formaPF;

    /**
     * Obtiene el valor de la propiedad numeroF.
     * 
     */
    public int getNumeroF() {
        return numeroF;
    }

    /**
     * Define el valor de la propiedad numeroF.
     * 
     */
    public void setNumeroF(int value) {
        this.numeroF = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaF.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaF() {
        return fechaF;
    }

    /**
     * Define el valor de la propiedad fechaF.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaF(String value) {
        this.fechaF = value;
    }

    /**
     * Obtiene el valor de la propiedad montoF.
     * 
     */
    public int getMontoF() {
        return montoF;
    }

    /**
     * Define el valor de la propiedad montoF.
     * 
     */
    public void setMontoF(int value) {
        this.montoF = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoC() {
        return codigoC;
    }

    /**
     * Define el valor de la propiedad codigoC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoC(String value) {
        this.codigoC = value;
    }

    /**
     * Obtiene el valor de la propiedad formaPF.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormaPF() {
        return formaPF;
    }

    /**
     * Define el valor de la propiedad formaPF.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormaPF(String value) {
        this.formaPF = value;
    }

}
