
package webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AgregarPago complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AgregarPago"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="numeroCP" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="fechaP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="montoP" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="codigoC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="tipoP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgregarPago", propOrder = {
    "numeroCP",
    "fechaP",
    "montoP",
    "codigoC",
    "tipoP"
})
public class AgregarPago {

    protected int numeroCP;
    protected String fechaP;
    protected int montoP;
    protected String codigoC;
    protected String tipoP;

    /**
     * Obtiene el valor de la propiedad numeroCP.
     * 
     */
    public int getNumeroCP() {
        return numeroCP;
    }

    /**
     * Define el valor de la propiedad numeroCP.
     * 
     */
    public void setNumeroCP(int value) {
        this.numeroCP = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaP.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaP() {
        return fechaP;
    }

    /**
     * Define el valor de la propiedad fechaP.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaP(String value) {
        this.fechaP = value;
    }

    /**
     * Obtiene el valor de la propiedad montoP.
     * 
     */
    public int getMontoP() {
        return montoP;
    }

    /**
     * Define el valor de la propiedad montoP.
     * 
     */
    public void setMontoP(int value) {
        this.montoP = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoC() {
        return codigoC;
    }

    /**
     * Define el valor de la propiedad codigoC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoC(String value) {
        this.codigoC = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoP.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoP() {
        return tipoP;
    }

    /**
     * Define el valor de la propiedad tipoP.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoP(String value) {
        this.tipoP = value;
    }

}
