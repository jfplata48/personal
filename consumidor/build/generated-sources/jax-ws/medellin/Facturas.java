
package medellin;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para facturas complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="facturas"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="codigoC" type="{http://medellin/}clientes" minOccurs="0"/&gt;
 *         &lt;element name="fechaF" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="formaPF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="montoF" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="numeroF" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "facturas", propOrder = {
    "codigoC",
    "fechaF",
    "formaPF",
    "montoF",
    "numeroF"
})
public class Facturas {

    protected Clientes codigoC;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaF;
    protected String formaPF;
    protected int montoF;
    protected Integer numeroF;

    /**
     * Obtiene el valor de la propiedad codigoC.
     * 
     * @return
     *     possible object is
     *     {@link Clientes }
     *     
     */
    public Clientes getCodigoC() {
        return codigoC;
    }

    /**
     * Define el valor de la propiedad codigoC.
     * 
     * @param value
     *     allowed object is
     *     {@link Clientes }
     *     
     */
    public void setCodigoC(Clientes value) {
        this.codigoC = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaF.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaF() {
        return fechaF;
    }

    /**
     * Define el valor de la propiedad fechaF.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaF(XMLGregorianCalendar value) {
        this.fechaF = value;
    }

    /**
     * Obtiene el valor de la propiedad formaPF.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormaPF() {
        return formaPF;
    }

    /**
     * Define el valor de la propiedad formaPF.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormaPF(String value) {
        this.formaPF = value;
    }

    /**
     * Obtiene el valor de la propiedad montoF.
     * 
     */
    public int getMontoF() {
        return montoF;
    }

    /**
     * Define el valor de la propiedad montoF.
     * 
     */
    public void setMontoF(int value) {
        this.montoF = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroF.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroF() {
        return numeroF;
    }

    /**
     * Define el valor de la propiedad numeroF.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroF(Integer value) {
        this.numeroF = value;
    }

}
