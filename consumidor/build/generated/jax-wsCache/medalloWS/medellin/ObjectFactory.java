
package medellin;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the medellin package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Clientes_QNAME = new QName("http://medellin/", "clientes");
    private final static QName _ConsultaClientesM_QNAME = new QName("http://medellin/", "consultaClientesM");
    private final static QName _ConsultaClientesMResponse_QNAME = new QName("http://medellin/", "consultaClientesMResponse");
    private final static QName _ConsultaSaldoCM_QNAME = new QName("http://medellin/", "consultaSaldoCM");
    private final static QName _ConsultaSaldoCMResponse_QNAME = new QName("http://medellin/", "consultaSaldoCMResponse");
    private final static QName _Facturas_QNAME = new QName("http://medellin/", "facturas");
    private final static QName _ListaFacturasCS0_QNAME = new QName("http://medellin/", "listaFacturasCS0");
    private final static QName _ListaFacturasCS0Response_QNAME = new QName("http://medellin/", "listaFacturasCS0Response");
    private final static QName _ListaPagosCM_QNAME = new QName("http://medellin/", "listaPagosCM");
    private final static QName _ListaPagosCMResponse_QNAME = new QName("http://medellin/", "listaPagosCMResponse");
    private final static QName _Pagos_QNAME = new QName("http://medellin/", "pagos");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: medellin
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Clientes }
     * 
     */
    public Clientes createClientes() {
        return new Clientes();
    }

    /**
     * Create an instance of {@link ConsultaClientesM }
     * 
     */
    public ConsultaClientesM createConsultaClientesM() {
        return new ConsultaClientesM();
    }

    /**
     * Create an instance of {@link ConsultaClientesMResponse }
     * 
     */
    public ConsultaClientesMResponse createConsultaClientesMResponse() {
        return new ConsultaClientesMResponse();
    }

    /**
     * Create an instance of {@link ConsultaSaldoCM }
     * 
     */
    public ConsultaSaldoCM createConsultaSaldoCM() {
        return new ConsultaSaldoCM();
    }

    /**
     * Create an instance of {@link ConsultaSaldoCMResponse }
     * 
     */
    public ConsultaSaldoCMResponse createConsultaSaldoCMResponse() {
        return new ConsultaSaldoCMResponse();
    }

    /**
     * Create an instance of {@link Facturas }
     * 
     */
    public Facturas createFacturas() {
        return new Facturas();
    }

    /**
     * Create an instance of {@link ListaFacturasCS0 }
     * 
     */
    public ListaFacturasCS0 createListaFacturasCS0() {
        return new ListaFacturasCS0();
    }

    /**
     * Create an instance of {@link ListaFacturasCS0Response }
     * 
     */
    public ListaFacturasCS0Response createListaFacturasCS0Response() {
        return new ListaFacturasCS0Response();
    }

    /**
     * Create an instance of {@link ListaPagosCM }
     * 
     */
    public ListaPagosCM createListaPagosCM() {
        return new ListaPagosCM();
    }

    /**
     * Create an instance of {@link ListaPagosCMResponse }
     * 
     */
    public ListaPagosCMResponse createListaPagosCMResponse() {
        return new ListaPagosCMResponse();
    }

    /**
     * Create an instance of {@link Pagos }
     * 
     */
    public Pagos createPagos() {
        return new Pagos();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Clientes }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Clientes }{@code >}
     */
    @XmlElementDecl(namespace = "http://medellin/", name = "clientes")
    public JAXBElement<Clientes> createClientes(Clientes value) {
        return new JAXBElement<Clientes>(_Clientes_QNAME, Clientes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaClientesM }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ConsultaClientesM }{@code >}
     */
    @XmlElementDecl(namespace = "http://medellin/", name = "consultaClientesM")
    public JAXBElement<ConsultaClientesM> createConsultaClientesM(ConsultaClientesM value) {
        return new JAXBElement<ConsultaClientesM>(_ConsultaClientesM_QNAME, ConsultaClientesM.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaClientesMResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ConsultaClientesMResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://medellin/", name = "consultaClientesMResponse")
    public JAXBElement<ConsultaClientesMResponse> createConsultaClientesMResponse(ConsultaClientesMResponse value) {
        return new JAXBElement<ConsultaClientesMResponse>(_ConsultaClientesMResponse_QNAME, ConsultaClientesMResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaSaldoCM }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ConsultaSaldoCM }{@code >}
     */
    @XmlElementDecl(namespace = "http://medellin/", name = "consultaSaldoCM")
    public JAXBElement<ConsultaSaldoCM> createConsultaSaldoCM(ConsultaSaldoCM value) {
        return new JAXBElement<ConsultaSaldoCM>(_ConsultaSaldoCM_QNAME, ConsultaSaldoCM.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaSaldoCMResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ConsultaSaldoCMResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://medellin/", name = "consultaSaldoCMResponse")
    public JAXBElement<ConsultaSaldoCMResponse> createConsultaSaldoCMResponse(ConsultaSaldoCMResponse value) {
        return new JAXBElement<ConsultaSaldoCMResponse>(_ConsultaSaldoCMResponse_QNAME, ConsultaSaldoCMResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Facturas }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Facturas }{@code >}
     */
    @XmlElementDecl(namespace = "http://medellin/", name = "facturas")
    public JAXBElement<Facturas> createFacturas(Facturas value) {
        return new JAXBElement<Facturas>(_Facturas_QNAME, Facturas.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListaFacturasCS0 }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ListaFacturasCS0 }{@code >}
     */
    @XmlElementDecl(namespace = "http://medellin/", name = "listaFacturasCS0")
    public JAXBElement<ListaFacturasCS0> createListaFacturasCS0(ListaFacturasCS0 value) {
        return new JAXBElement<ListaFacturasCS0>(_ListaFacturasCS0_QNAME, ListaFacturasCS0 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListaFacturasCS0Response }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ListaFacturasCS0Response }{@code >}
     */
    @XmlElementDecl(namespace = "http://medellin/", name = "listaFacturasCS0Response")
    public JAXBElement<ListaFacturasCS0Response> createListaFacturasCS0Response(ListaFacturasCS0Response value) {
        return new JAXBElement<ListaFacturasCS0Response>(_ListaFacturasCS0Response_QNAME, ListaFacturasCS0Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListaPagosCM }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ListaPagosCM }{@code >}
     */
    @XmlElementDecl(namespace = "http://medellin/", name = "listaPagosCM")
    public JAXBElement<ListaPagosCM> createListaPagosCM(ListaPagosCM value) {
        return new JAXBElement<ListaPagosCM>(_ListaPagosCM_QNAME, ListaPagosCM.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListaPagosCMResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ListaPagosCMResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://medellin/", name = "listaPagosCMResponse")
    public JAXBElement<ListaPagosCMResponse> createListaPagosCMResponse(ListaPagosCMResponse value) {
        return new JAXBElement<ListaPagosCMResponse>(_ListaPagosCMResponse_QNAME, ListaPagosCMResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Pagos }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Pagos }{@code >}
     */
    @XmlElementDecl(namespace = "http://medellin/", name = "pagos")
    public JAXBElement<Pagos> createPagos(Pagos value) {
        return new JAXBElement<Pagos>(_Pagos_QNAME, Pagos.class, null, value);
    }

}
