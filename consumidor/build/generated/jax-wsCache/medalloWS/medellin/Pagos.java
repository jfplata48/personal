
package medellin;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para pagos complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="pagos"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="codigoC" type="{http://medellin/}clientes" minOccurs="0"/&gt;
 *         &lt;element name="fechaP" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="montoP" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="numeroCP" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="tipoP" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pagos", propOrder = {
    "codigoC",
    "fechaP",
    "montoP",
    "numeroCP",
    "tipoP"
})
public class Pagos {

    protected Clientes codigoC;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaP;
    protected int montoP;
    protected Integer numeroCP;
    @XmlSchemaType(name = "unsignedShort")
    protected Integer tipoP;

    /**
     * Obtiene el valor de la propiedad codigoC.
     * 
     * @return
     *     possible object is
     *     {@link Clientes }
     *     
     */
    public Clientes getCodigoC() {
        return codigoC;
    }

    /**
     * Define el valor de la propiedad codigoC.
     * 
     * @param value
     *     allowed object is
     *     {@link Clientes }
     *     
     */
    public void setCodigoC(Clientes value) {
        this.codigoC = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaP.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaP() {
        return fechaP;
    }

    /**
     * Define el valor de la propiedad fechaP.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaP(XMLGregorianCalendar value) {
        this.fechaP = value;
    }

    /**
     * Obtiene el valor de la propiedad montoP.
     * 
     */
    public int getMontoP() {
        return montoP;
    }

    /**
     * Define el valor de la propiedad montoP.
     * 
     */
    public void setMontoP(int value) {
        this.montoP = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroCP.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroCP() {
        return numeroCP;
    }

    /**
     * Define el valor de la propiedad numeroCP.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroCP(Integer value) {
        this.numeroCP = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoP.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTipoP() {
        return tipoP;
    }

    /**
     * Define el valor de la propiedad tipoP.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTipoP(Integer value) {
        this.tipoP = value;
    }

}
