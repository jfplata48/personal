
package webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AgregarUsuario complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AgregarUsuario"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="codigoC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nombreC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="addrC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="saldoC" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgregarUsuario", propOrder = {
    "codigoC",
    "nombreC",
    "addrC",
    "saldoC"
})
public class AgregarUsuario {

    protected String codigoC;
    protected String nombreC;
    protected String addrC;
    protected int saldoC;

    /**
     * Obtiene el valor de la propiedad codigoC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoC() {
        return codigoC;
    }

    /**
     * Define el valor de la propiedad codigoC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoC(String value) {
        this.codigoC = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreC() {
        return nombreC;
    }

    /**
     * Define el valor de la propiedad nombreC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreC(String value) {
        this.nombreC = value;
    }

    /**
     * Obtiene el valor de la propiedad addrC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddrC() {
        return addrC;
    }

    /**
     * Define el valor de la propiedad addrC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddrC(String value) {
        this.addrC = value;
    }

    /**
     * Obtiene el valor de la propiedad saldoC.
     * 
     */
    public int getSaldoC() {
        return saldoC;
    }

    /**
     * Define el valor de la propiedad saldoC.
     * 
     */
    public void setSaldoC(int value) {
        this.saldoC = value;
    }

}
