
package webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AgregarFactura_QNAME = new QName("http://webservice/", "AgregarFactura");
    private final static QName _AgregarFacturaResponse_QNAME = new QName("http://webservice/", "AgregarFacturaResponse");
    private final static QName _AgregarPago_QNAME = new QName("http://webservice/", "AgregarPago");
    private final static QName _AgregarPagoResponse_QNAME = new QName("http://webservice/", "AgregarPagoResponse");
    private final static QName _AgregarUsuario_QNAME = new QName("http://webservice/", "AgregarUsuario");
    private final static QName _AgregarUsuarioResponse_QNAME = new QName("http://webservice/", "AgregarUsuarioResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AgregarFactura }
     * 
     */
    public AgregarFactura createAgregarFactura() {
        return new AgregarFactura();
    }

    /**
     * Create an instance of {@link AgregarFacturaResponse }
     * 
     */
    public AgregarFacturaResponse createAgregarFacturaResponse() {
        return new AgregarFacturaResponse();
    }

    /**
     * Create an instance of {@link AgregarPago }
     * 
     */
    public AgregarPago createAgregarPago() {
        return new AgregarPago();
    }

    /**
     * Create an instance of {@link AgregarPagoResponse }
     * 
     */
    public AgregarPagoResponse createAgregarPagoResponse() {
        return new AgregarPagoResponse();
    }

    /**
     * Create an instance of {@link AgregarUsuario }
     * 
     */
    public AgregarUsuario createAgregarUsuario() {
        return new AgregarUsuario();
    }

    /**
     * Create an instance of {@link AgregarUsuarioResponse }
     * 
     */
    public AgregarUsuarioResponse createAgregarUsuarioResponse() {
        return new AgregarUsuarioResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarFactura }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AgregarFactura }{@code >}
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "AgregarFactura")
    public JAXBElement<AgregarFactura> createAgregarFactura(AgregarFactura value) {
        return new JAXBElement<AgregarFactura>(_AgregarFactura_QNAME, AgregarFactura.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarFacturaResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AgregarFacturaResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "AgregarFacturaResponse")
    public JAXBElement<AgregarFacturaResponse> createAgregarFacturaResponse(AgregarFacturaResponse value) {
        return new JAXBElement<AgregarFacturaResponse>(_AgregarFacturaResponse_QNAME, AgregarFacturaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarPago }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AgregarPago }{@code >}
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "AgregarPago")
    public JAXBElement<AgregarPago> createAgregarPago(AgregarPago value) {
        return new JAXBElement<AgregarPago>(_AgregarPago_QNAME, AgregarPago.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarPagoResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AgregarPagoResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "AgregarPagoResponse")
    public JAXBElement<AgregarPagoResponse> createAgregarPagoResponse(AgregarPagoResponse value) {
        return new JAXBElement<AgregarPagoResponse>(_AgregarPagoResponse_QNAME, AgregarPagoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarUsuario }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AgregarUsuario }{@code >}
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "AgregarUsuario")
    public JAXBElement<AgregarUsuario> createAgregarUsuario(AgregarUsuario value) {
        return new JAXBElement<AgregarUsuario>(_AgregarUsuario_QNAME, AgregarUsuario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarUsuarioResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AgregarUsuarioResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "AgregarUsuarioResponse")
    public JAXBElement<AgregarUsuarioResponse> createAgregarUsuarioResponse(AgregarUsuarioResponse value) {
        return new JAXBElement<AgregarUsuarioResponse>(_AgregarUsuarioResponse_QNAME, AgregarUsuarioResponse.class, null, value);
    }

}
