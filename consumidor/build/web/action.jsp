<%-- 
    Document   : action
    Created on : 5/06/2018, 09:52:44 PM
    Author     : julia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Respuesta</h1>
    
    <%-- start web service invocation --%><hr/>
    <%
    try {
	medellin.MedalloWS_Service service = new medellin.MedalloWS_Service();
	medellin.MedalloWS port = service.getMedalloWSPort();
	// TODO process result here
	java.util.List<medellin.Clientes> result = port.consultaClientesM();
        out.println("Result = ");
        for (int i=0;i<result.size();i++){
	out.println(result.get(i));
        }
    } catch (Exception ex) {
	// TODO handle custom exceptions here
    }
    %>
    <%-- end web service invocation --%><hr/>
    </body>
</html>
