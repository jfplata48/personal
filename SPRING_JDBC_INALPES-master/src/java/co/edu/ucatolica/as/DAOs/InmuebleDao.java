/**
 * Este es la clase con todas las operaciones de acceso a los datos
 */
package co.edu.ucatolica.as.DAOs;

import co.edu.ucatolica.as.DTOs.Inmueble;
import co.edu.ucatolica.as.DTOs.Persona;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author CRISTIAN_OMEN
 */
public class InmuebleDao {
    public boolean crearInmueble(Inmueble p, Connection con)
    {
        PreparedStatement pstmt = null;
        boolean respuesta = false;
        try {            
            
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando crearPersona...");
            
            pstmt = con.prepareStatement("INSERT INTO inmueble ( idinmueble,nombre_inmueble, "
                    + " Tipo_inmueble_idtipo_inmueble, Sucursales_idsucursales) "
                    + " VALUES (?,?,?,?)");
            
           
       
            pstmt.setInt(1, p.getId());
            pstmt.setString(2, p.getNombre_inmueble());
            pstmt.setString(3, p.getTipo_inmueble_idtipo_inmueble());
            pstmt.setString(4, p.getSucursales_idsucursales());
            
            pstmt.execute();
            
            con.close();
            
            respuesta = true;
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return respuesta;

    }

    public ArrayList<Inmueble> consultarInmueble (Inmueble p, Connection con)
    {
        
        ArrayList<Inmueble> datos = new ArrayList();
        
        Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando consultarPersona...");
        
        try {
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery ("select idinmueble,nombre_inmueble, "
                    + " Tipo_inmueble_idtipo_inmueble, Sucursales_idsucursales "
                    + " from inmueble "
                    + " where "
                    + " idinmueble='" + p.getId()+"'"
                    + " AND nombre_inmueble='"+p.getNombre_inmueble()+"'");
            
            while (rs.next())
            { 
                Inmueble per = new Inmueble();
                per.setId(rs.getInt(1));
                per.setNombre_inmueble(rs.getString(2));
                per.setTipo_inmueble_idtipo_inmueble(rs.getString(3));
                per.setSucursales_idsucursales(rs.getString(4));             
                datos.add(per);
                
            }
            
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando consultarInmueble fin..." + datos.size());
            
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return datos;
    }
     public ArrayList<Inmueble> consultarInmuebles(Connection con)
    {
        
        ArrayList<Inmueble> datos = new ArrayList();
        
        Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando consultarPersona...");
        
        try {
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery ("select idinmueble,nombre_inmueble, "
                    + " Tipo_inmueble_idtipo_inmueble, Sucursales_idsucursales "
                    + " from inmueble ");
            
            while (rs.next())
            { 
                Inmueble per = new Inmueble();
                per.setId(rs.getInt(1));
                per.setNombre_inmueble(rs.getString(2));
                per.setTipo_inmueble_idtipo_inmueble(rs.getString(3));
                per.setSucursales_idsucursales(rs.getString(4));             
                datos.add(per);
                
            }
            
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando consultarInmueble fin..." + datos.size());
            
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return datos;
    }
    
    
    
         
    public boolean editarInmueble(Inmueble p, Connection con)
    {
        PreparedStatement pstmt = null;
        boolean respuesta = false;
        try {            
            
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando editarInmueble...");
            
            pstmt = con.prepareStatement("UPDATE inmueble "
                    + " SET "
                    + "nombre_inmueble=?"
                    + ",Tipo_inmueble_idtipo_inmueble=?"
                    + ",Sucursales_idsucursales=?"
                    + " where idinmueble=?");
            
            pstmt.setString(1, p.getNombre_inmueble());
            pstmt.setString(2, p.getTipo_inmueble_idtipo_inmueble());
            pstmt.setString(3, p.getSucursales_idsucursales());
            pstmt.setInt(4, p.getId());

            pstmt.executeUpdate();            
            con.close();
            
            respuesta = true;
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return respuesta;

    }
    
}
