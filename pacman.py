import numpy as N
import sys
import time

f = 0
c = 0
dx = [-1, 0, 1, 0]
dy = [0, 1, 0, -1]
prototipo = [['@', '.', '.', '.', '.', '.'],
             ['.', '#', '.', '#', '#', '.'],
             ['.', '#', '.', 'F', '#', 'M'],
             ['.', '#', '.', '#', '#', '.'],
             ['.', '.', '.', '.', '.', '.']]
mattem = prototipo
PDS=[]
posicionp=[]
posicionf=[]
dimx=0
dimy=0
def dimension():
    dimx,dimy=N.shape(prototipo)
    return dimx,dimy

def imprimirMatriz():
    """print("Remember: ")
    print("1.You only can move Up (0),Right(1) , Down(2) and Left(3) ")
    print("2.You can't trespass walls '#'")
    print("3.Careful, do not take the ghost 'F'! you will die :C")
    print("4.Meet with Mrs.Pac-Man 'M'")
    print("4.you are this guy: '@'")
    print("Tip: You walked around here '*', and the ghost walked around here 'o'")"""
    for i in range(dimx):
        for j in range(dimy):
            print('[', prototipo[i][j], ']', end ='')
        print()
    print("------")


def finding(x):
    for i in range(dimx):
        for j in range(dimy):
            if (prototipo[i][j] == x):
                return i, j
class AEstrella:
    def __init__(self, laberinto):
        self.laberinto = laberinto
        self.pos_final = buscarPosicion('@', self.laberinto)
        self.inicio = Nodo(self.pos_final, buscarPosicion('F', laberinto), None)
        self.fin = Nodo(self.pos_final, self.pos_final, None)
        self.abierta = []
        self.cerrada = []
        self.cerrada.append(self.inicio)
        self.abierta += self.vecinos(self.inicio)
        while self.objetivo():
            self.buscar()
        self.camino = self.camino()
    def vecinos(self, nodo):
        vecinos = []
        try:
            if self.laberinto[nodo.posicion[0] + 1][nodo.posicion[1]] != '0':
                vecinos.append(Nodo(self.pos_final, [nodo.posicion[0] + 1, nodo.posicion[1]], nodo))
        except IndexError:
            pass
        try:
            if self.laberinto[nodo.posicion[0] - 1][nodo.posicion[1]] != '0':
                vecinos.append(Nodo(self.pos_final, [nodo.posicion[0] - 1, nodo.posicion[1]], nodo))
        except IndexError:
            pass
        try:
            if self.laberinto[nodo.posicion[0]][nodo.posicion[1] - 1] != '0':
                vecinos.append(Nodo(self.pos_final, [nodo.posicion[0], nodo.posicion[1] - 1], nodo))
        except IndexError:
            pass
        try:
            if self.laberinto[nodo.posicion[0]][nodo.posicion[1] + 1] != '0':
                vecinos.append(Nodo(self.pos_final, [nodo.posicion[0], nodo.posicion[1] + 1], nodo))
        except IndexError:
            pass
        return vecinos
    def f_menor(self):
        actual = self.abierta[0]
        n = 0
        for i in range(1, len(self.abierta)):
            if self.abierta[i].f < actual.f:
                actual = self.abierta[i]
                n = i
        self.cerrada.append(self.abierta[n])
        del self.abierta[n]
    def exists(self, nodo, lista):
        for i in range(len(lista)):
            if nodo.posicion == lista[i].posicion:
                return True
        return False
    def ruta(self):
        for i in range(len(self.nodos)):
            if self.exists(self.nodos[i], self.cerrada):
                continue
            elif not self.exists(self.nodos[i], self.abierta):
                self.abierta.append(self.nodos[i])
            else:
                if self.select.g + 1 < self.nodos[i].g:
                    for j in range(len(self.abierta)):
                        if self.nodos[i].posicion == self.abierta[j].posicion:
                            del self.abierta[j]
                            self.abierta.append(self.nodos[i])
                            break
    def buscar(self):
        self.f_menor()
        self.select = self.cerrada[-1]
        self.nodos = self.vecinos(self.select)
        self.ruta()
    def objetivo(self):
        for i in range(len(self.abierta)):
            if self.fin.posicion == self.abierta[i].posicion:
                return False
        return True
    def camino(self):
        print
        len(self.abierta)
        for i in range(len(self.abierta)):
            if self.fin.posicion == self.abierta[i].posicion:
                objetivo = self.abierta[i]
        camino = []
        while objetivo.padre != None:
            camino.append(objetivo.posicion)
            objetivo = objetivo.padre
        camino.reverse()
        return camino
class Nodo:
    def __init__(self, pos_final, posicion=[0, 0], padre=None):
        self.posicion = posicion
        self.padre = padre
        self.h = manhattan(self.posicion, pos_final)
        if self.padre == None:
            self.g = 0
        else:
            self.g = self.padre.g + 1
        self.f = self.g + self.h
def manhattan(a, b):
    return abs(a[0] - b[0]) + abs(a[1] - b[1])

def buscarPosicion(x, laberinto):
    for fila in range(len(laberinto)):
        for columna in range(len(laberinto[0])):
            if laberinto[fila][columna] == x:
                return [fila, columna]
def main():
    laberinto = mattem
    print("inicio %s " % buscarPosicion('F', laberinto))
    print(    "final %s " % buscarPosicion('@', laberinto))
    algoritmo = AEstrella(laberinto)
    print(algoritmo.camino)
    return (algoritmo.camino)
def solucionalmacenada():
    t=main()
    PDS.append(t)
    print (PDS)
def movfan():
    f,c=finding('F')
    if (f== None and c==None ):
        f,c=finding('FM')
    for q in range (4):
        movx= f+dx[q]
        movy= c+dy[q]
        if (movx < dimx and movx >= 0 and movy < dimy and movy >= 0):
            if (PDS[len(PDS)-1][0]==[movx,movy]):
                print ("posicion",movx,movy)
                if (prototipo[f][c]=='FM'):
                    prototipo[f][c]='M'
                    
                posicionf.append([movx,movy])
                if (prototipo[movx][movy] == '.' or prototipo[movx][movy] == '*' or prototipo[movx][movy] == 'o' or prototipo[movx][movy] == '#'):
                    print("entre")
                    prototipo[movx][movy]= 'F'
                    posicionf.append([movx,movy])
                    return 0
                elif(prototipo[f][c]=='F'):
                    prototipo[f][c]='o'
                elif (prototipo[movx][movy]=='M'):
                    prototipo[movx][movy]= 'FM'                    
                elif(prototipo[movx][movy]== '@'):
                    prototipo[movx][movy]= '@F'
                    print("You lose")
                    exit()
        
    
def movpac(a):
    f, c = finding('@')
    movx = f + dx[a]
    movy = c + dy[a]
    if (movx < dimx and movx >= 0 and movy < dimy and movy >= 0):
        if (prototipo[movx][movy] == '#'):
            print("it's a wall, you can't trespass it |:<")
            juego()
        prototipo[f][c] = '*'
        if (prototipo[movx][movy] == '.' or prototipo[movx][movy] == '*' or prototipo[movx][movy] == 'o' or prototipo[movx][movy] == 'o*'):
            prototipo[movx][movy] = '@'
            posicionp.append([movx,movy])
            solucionalmacenada()
            movfan()
            return 0
        elif (prototipo[movx][movy] == 'M'):
            prototipo[movx][movy] = '@M'
            return 1
        elif(prototipo[movx][movy]== 'F'):
            prototipo[movx][movy]= '@F'
            print("You lose")
            exit()
    else:
        print("Remember that you only can move in the board, not outside of it!")
        juego()


def juego():
    imprimirMatriz()
    a = int(input("Please input your move"))
    if (a >= 0 and a < 4):
        t = movpac(a)
    else:
        print("Hey! You have inputted a wrong choice, do it again")
        juego()
    if (t == 1):
        print("Congratulations, you did it :D")
        for i in range(dimx):
            for j in range(dimy):
                print('[', prototipo[i][j], ']', end='')
            print()
        return 0
    else:
        juego()

dimx,dimy = dimension()

print("Start Game")
juego()
